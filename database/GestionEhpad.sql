CREATE TABLE `Establishments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

INSERT INTO `Establishments` (`id`, `name`, `city`) VALUES
(1,	'Domaine du soleil levant',	'Perpignan'),
(2,	'Maison de retraite',	'Clermont-Ferrand');

CREATE TABLE `Residents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(100) NOT NULL,
  `lastName` varchar(100) NOT NULL,
  `dateOfBirth` date NOT NULL,
  `establishmentId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `establishmentId` (`establishmentId`),
  CONSTRAINT `Residents_ibfk_1` FOREIGN KEY (`establishmentId`) REFERENCES `Establishments` (`id`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

INSERT INTO `Residents` (`id`, `firstName`, `lastName`, `dateOfBirth`, `establishmentId`) VALUES
(1,	'Marcel',	'Langer',	'1903-05-13',	2),
(2,	'Claude',	'Nougaro',	'1929-09-09',	2),
(3,	'Jean',	'Dupont',	'1940-01-03',	1),
(4,	'Aignan',	'Dupont',	'2021-08-03',	1),
(5,	'Aignan',	'Dupont',	'1923-05-17',	1),
(6,	'Dutronc',	'Marc',	'1976-04-15',	2),
(7,	'Domenech',	'Raymond',	'1963-09-04',	4),
(8,	'Jonas',	'Michel',	'1949-05-12',	6),
(9,	'Chirac',	'Jacques',	'1932-09-02',	5);

-- 2021-08-05 08:42:58
