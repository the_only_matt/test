const express = require('express')
const app = express()
const apiRoutes = require('./routes');
const database = require('./services/database')

database.init()

// Enable CORS
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
app.use(express.json());
app.use('/api', apiRoutes)

app.listen(3000, () => {
    console.log('App listening on port 3000');
});

app.on('close', function() {
    stop()
});

process.on('SIGINT', function() {
    stop()
});

const stop = () =>{
    database.close()
}