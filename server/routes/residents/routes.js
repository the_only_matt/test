const residentsRouter = require('express').Router({mergeParams: true});
const getResidentList = require('./GET')
const registerResident = require('./POST')

residentsRouter.get('/', async (req, res) => {
    const response = await getResidentList()
    res.status(response.status).json((response.data || response.error) || {});
});

residentsRouter.post('/', async (req, res) => {
    const response = await registerResident(req.body)
    res.status(response.status).json((response.data || response.error) || {});
});

module.exports = residentsRouter;