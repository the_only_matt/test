const database = require("./../../services/database");
const escape = require('mysql').escape;

module.exports = async (body) => {
    const response = {
        data : null,
        status : null,
        error : null,
    };

    if(body.constructor.name === 'String'){
        try{
            body = JSON.parse(body)
        }
        catch (e) {
            response.status = 400;
            response.error = 'ERR_PARAMS : Please send a valid JSON';
            return response;
        }
    }

    if(!body.firstName || !body.lastName || !body.dateOfBirth || !body.establishmentId){
        response.status = 400;
        response.error = 'ERR_PARAMS : Please set params firstName, lastName and dateOfBirth, establishmentId';
        return response;
    }

    if(Number.isNaN(Date.parse(body.dateOfBirth)) || new Date(body.dateOfBirth) > new Date()){
        response.status = 422;
        response.error = 'ERR_PARAMS_TYPE : Please set a valid date of birth';
        return response;
    }

    if(!parseInt(body.establishmentId)){
        response.status = 422;
        response.error = 'ERR_PARAMS_TYPE : Please set a valid establishment id';
        return response;
    }

    const query = `INSERT INTO Residents (firstName, lastName, dateOfBirth, establishmentId) 
                    VALUES (${escape(body.firstName)}, ${escape(body.lastName)}, ${escape(body.dateOfBirth)}, ${body.establishmentId})`;

    try{
        await database.query(query);
        response.status = 200;
    }
    catch (e) {
        console.error(e);
        response.status = 500;
        response.error = 'INTERNAL_SERVER_ERROR';
    }

    return response;
}
