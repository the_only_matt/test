const express = require('express')
const apiRoutes = express.Router({mergeParams: true});
const establishmentsRouter = require('./establishments/routes')
const residentsRouter = require('./residents/routes')

apiRoutes.use('/establishments', establishmentsRouter);
apiRoutes.use('/residents', residentsRouter);

module.exports = apiRoutes;