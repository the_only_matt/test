const database = require("./../../services/database");
const escape = require('mysql').escape;

module.exports = async (body) => {
    const response = {
        data : null,
        status : null,
        error : null,
    };

    if(!body.name || !body.city){
        response.status = 400;
        response.error = 'ERR_PARAMS : Please set params name and city';
        return response;
    }

    const query = `INSERT INTO Establishments (name, city) VALUES (${escape(body.name)}, ${escape(body.city)})`;

    try{
        await database.query(query);
        response.status = 200;
    }
    catch (e) {
        console.error(e);
        response.status = 500;
        response.error = 'INTERNAL_SERVER_ERROR';
    }

    return response;
}
