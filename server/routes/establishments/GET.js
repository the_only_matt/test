const database = require("./../../services/database");

module.exports = async () => {
    const response = {
        data : null,
        status : null,
        error : null,
    };

    try{
        response.data = await database.query('SELECT * FROM Establishments');
        response.status = 200;
    }
    catch (e) {
        console.error(e);
        response.status = 500;
        response.error = 'INTERNAL_SERVER_ERROR';
        return response;
    }

    return response;
}
