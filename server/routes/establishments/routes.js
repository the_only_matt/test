const database = require('./../../services/database')

const establishmentRouter = require('express').Router({mergeParams: true});
const getEstablishmentList = require('./GET')
const createEstablishment = require('./POST')

establishmentRouter.get('/', async (req, res) => {
    const response = await getEstablishmentList()
    res.status(response.status).json((response.data || response.error) || {});
});

establishmentRouter.post('/', async (req, res) => {
    const response = await createEstablishment(req.body)
    res.status(response.status).json((response.data || response.error) || {});
});

module.exports = establishmentRouter;