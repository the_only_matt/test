const mysql = require('mysql');
const {promisify} = require('util');
require('dotenv').config();

let db;

module.exports = {
    init: () => {
        db = mysql.createPool({
            connectionLimit : 10,
            host: process.env.MYSQL_HOST,
            port: process.env.MYSQL_PORT,
            database: 'GestionEhpad',
            user: process.env.MYSQL_USER,
            password: process.env.MYSQL_PASSWORD
        });
    },
    query: (query) => {
        return promisify(db.query).call(db, query);
    },
    close: () => {
        if (db) {
            db.end();
            db = undefined;
        }
    }
};