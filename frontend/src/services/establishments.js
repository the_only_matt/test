export default {
    async getList() {
        let data

        try {
            const response = await fetch(`${process.env.VUE_APP_API_URL}/establishments`)
            if (!response.ok) {
                console.error("An error occurred retrieving establishment list")
            }

            data = await response.json()
        } catch (e) {
            console.error(e)
        }

        return data
    },

    async create(params){
        const headers = new Headers()
        headers.append("Content-Type", "application/json")

        return await fetch(`${process.env.VUE_APP_API_URL}/establishments`, {
            method: "POST",
            body: params,
            headers
        })
    }
}