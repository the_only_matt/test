export default {
    async registerResident(params){
        const headers = new Headers()
        headers.append("Content-Type", "application/json")

        return await fetch(`${process.env.VUE_APP_API_URL}/residents`, {
            method: "POST",
            body : params,
            headers
        })
    }
}