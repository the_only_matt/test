import { createApp } from 'vue'
import App from './App.vue'
import './assets/styles.css';
import './assets/semantic.min.css';

const app = createApp(App).mount('#app')
export default app;