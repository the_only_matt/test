# List of commands to start the apps : 

### Create an image for the front-end
```
docker build -t mbeauvin.frontend ./frontend
```

### Create an image for the back-end
```
docker build -t mbeauvin.node-server ./server
```

### Use Docker Compose to start containers 
```
docker compose up
```

### App URL : 
```
http://127.0.0.1:8080
```